# [OT]HELLO #

This is "othello" game that can join between the same server of two people. It's a board game  that practices skill of planning.

### Rule ###

* 1st Player will be set "BLACK" and can start.
* 2nd Player will be set "WHITE".
* Player must strike the area the other.

### Feature ###
* Player can login to be player.
* Can play on the same server.
* Use OCSF server.
* Use MVC Pattern.

### Member ###
* [Jidapar Jettananurak](https://bitbucket.org/b5710546542)
* [Salilthip Phuklang](https://bitbucket.org/b5710546640)