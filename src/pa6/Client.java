
package pa6;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import com.lloseng.ocsf.client.AbstractClient;
/**
 * Ii is class for player use this application on server.
 * @author Jidapar Jettananurak
 *
 */
public class Client extends AbstractClient{

	private Controller controller;
	private String[] list;
	private int state;
	/**
	 * Initial client for connection with server.
	 * @param host is ip address of server
	 * @param port is number of port for connection
	 */
	public Client(String host, int port) {
		super(host, port);
		list = new String[3];
		controller = new Controller();
		controller.run();
	}

	/**
	 * Receive order from server and work.
	 */
	protected void handleMessageFromServer(Object msg){
		String s = (String) msg;
		list = s.split(",");
		
		if(list[0].equals("PLAYER1")){
			state = 1;
		}else if(list[0].equals("PLAYER2")){
			state = 2;
		}else if(controller.getPlayer1() == null){
			controller.newPlayer1(list[0]);
		}else if(controller.getPlayer2() == null){
			controller.newPlayer2(list[0]);
		}else if(list[0].equals("START") && controller.isFull()){
			controller.getStart();
			controller.getMenuUI().setVisible(false);
		}else if(list[0].equals("END")){
			System.out.println("game end");
			
			if(controller.getPlayer1().getScore() > controller.getPlayer2().getScore()){
				JOptionPane.showMessageDialog(null, "Player1 is win");
			}else if(controller.getPlayer2().getScore() > controller.getPlayer1().getScore()){
				JOptionPane.showMessageDialog(null, "Player2 is win");
			}else{
				JOptionPane.showMessageDialog(null, "EVEN");
			}
			
			try {
				sendToServer("gg");
				System.out.println("gg");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("catch");
			}
		}else if(list[0].equals("gg")){
			
		}else if(list[0].equals("1")){
			controller.updateBoard(Integer.parseInt(list[0]), Integer.parseInt(list[1]) , Integer.parseInt(list[2]));
			controller.getUI().setImage(controller.getBoard().getTable());
			controller.setScore();
			controller.getUI().setScoreLabel(controller.getPlayer1().getScore(),controller.getPlayer2().getScore());
			controller.checkEndGame();
			if(controller.isEndGame()){
				try {
					sendToServer("end");
					System.out.println("end");
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("exception");
				}
			}
			if(!controller.checkAllPosition(controller.getState() )){
				controller.print(controller.getBoard().getTable());
			}
		}else if(list[0].equals("2")){
			controller.updateBoard(Integer.parseInt(list[0]) , Integer.parseInt(list[1]) , Integer.parseInt(list[2]));
			controller.getUI().setImage(controller.getBoard().getTable());
			controller.setScore();
			controller.getUI().setScoreLabel(controller.getPlayer1().getScore(),controller.getPlayer2().getScore());
			controller.checkEndGame();
			if(controller.isEndGame()){
				try {
					sendToServer("end");
					System.out.println("end");
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("exception");
				}
			}
		}
	}

	public static void main(String[] args){
		String host = "192.168.1.107";
		int port = 5555;
		Client client = new Client(host, port);
		String str = "";
		while(true){
			try{
				client.openConnection();
				if( client.controller.getMenuUI().isPressLogin() ){
					str = client.controller.getMenuUI().getName().toString();
					client.controller.getMenuUI().setPressLogin(false);
					if(!str.equals("")){
						client.sendToServer(str);	
					}else JOptionPane.showMessageDialog(null, "Please, fill your name");
				}
				else if( client.controller.getMenuUI().isPressStart() ){
					str = "play";
					client.controller.getMenuUI().setPressStart(false);
					if(client.controller.isFull()){
						client.sendToServer(str);	
					}else JOptionPane.showMessageDialog(null, "Please, Login");
				}else if( client.controller.getUI() != null ){
					if(client.controller.getState() == client.state){
						if(client.controller.getUI().isSelecButton()){
							client.controller.getUI().setSelecButton(false);
							client.controller.updateBoard(client.controller.getState(), client.controller.getUI().getRow(), client.controller.getUI().getColumn());
							client.sendToServer(client.controller.getState() + "," + client.controller.getUI().getRow() + "," + client.controller.getUI().getColumn());
						}
					}
				}
			}catch(Exception e){
				System.out.println("Catch Exception");
			}
		}
	}

}
