package pa6;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ClientFrame
  extends Frame
{
  private static final int DEFAULT_PORT = 5555;
  private static final String DEFAULT_HOST = "localhost";
  private Button closeButton;
  private Button openButton;
  private Button sendButton;
  private Button quitButton;
  private TextField portField;
  private TextField hostField;
  private TextField messageField;
  private Label portLabel;
  private Label hostLabel;
  private Font font = new Font("Angsana New", 0, 16);
  private List liste;
  private Client client;
  private int port = 5555;
  private String host = "localhost";
  
  public ClientFrame(String host, int port)
  {
    super("Simple Client");
    if ((host != null) && (host.length() > 0)) {
      this.host = host;
    }
    if (port > 0) {
      this.port = port;
    }
    this.liste = new List();
    
    initComponents();
    
    this.client = new Client(host, port);
    this.portField.setText(String.valueOf(port));
    this.hostField.setText(host);
    
    addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        ClientFrame.this.quit();
      }
    });
    this.quitButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClientFrame.this.quit();
      }
    });
    this.closeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClientFrame.this.close();
      }
    });
    this.openButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClientFrame.this.open();
      }
    });
    ActionListener messageActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClientFrame.this.send();
      }
    };
    this.sendButton.addActionListener(messageActionListener);
    this.messageField.addActionListener(messageActionListener);
  }
  
  private void initComponents()
  {
    int gap = 5;
    
    Font messageFont = new Font("DialogInput", 0, 18);
    this.closeButton = new Button("Close");
    this.openButton = new Button("Open");
    this.sendButton = new Button("Send");
    this.quitButton = new Button("Quit");
    this.portField = new TextField(8);
    this.portField.setText(Integer.toString(this.port));
    this.hostField = new TextField(24);
    this.hostField.setText(this.host);
    this.messageField = new TextField();
    this.portLabel = new Label("Port:", 2);
    this.hostLabel = new Label("Host:", 2);
    
    Panel hostPanel = new Panel();
    hostPanel.setLayout(new FlowLayout(0, 5, 5));
    hostPanel.add(this.hostLabel);
    hostPanel.add(this.hostField);
    hostPanel.add(this.portLabel);
    hostPanel.add(this.portField);
    hostPanel.add(this.openButton);
    hostPanel.add(this.closeButton);
    
    Panel msgPanel = new Panel();
    msgPanel.setLayout(new GridLayout(1, 1));
    msgPanel.add(this.messageField);
    this.messageField.setFont(messageFont);
    
    Panel buttonPanel = new Panel();
    buttonPanel.setLayout(new GridLayout(1, 4, 5, 5));
    buttonPanel.add(this.sendButton);
    buttonPanel.add(this.quitButton);
    
    Panel southPanel = new Panel();
    southPanel.setLayout(new GridLayout(2, 1, 5, 5));
    southPanel.add(msgPanel);
    southPanel.add(buttonPanel);
    Component[] arrayOfComponent;
    int j = (arrayOfComponent = hostPanel.getComponents()).length;
    for (int i = 0; i < j; i++)
    {
      Component c = arrayOfComponent[i];c.setFont(this.font);
    }
    j = (arrayOfComponent = buttonPanel.getComponents()).length;
    for (int i = 0; i < j; i++)
    {
      Component c = arrayOfComponent[i];c.setFont(this.font);
    }
    this.liste.setFont(messageFont);
    
    setLayout(new BorderLayout(5, 5));
    
    add("North", hostPanel);
    
    add("Center", this.liste);
    
    add("South", southPanel);
    pack();
    setVisible(true);
  }
  
  private void readFields()
  {
    int p = Integer.parseInt(this.portField.getText());
    this.client.setPort(p);
    this.client.setHost(this.hostField.getText());
  }
  
  public void close()
  {
    try
    {
      readFields();
      this.client.closeConnection();
    }
    catch (Exception ex)
    {
      this.liste.add(ex.toString());
      this.liste.makeVisible(this.liste.getItemCount() - 1);
      this.liste.setBackground(Color.red);
    }
  }
  
  public void open()
  {
    try
    {
      readFields();
      this.client.openConnection();
    }
    catch (Exception ex)
    {
      this.liste.add(ex.toString());
      this.liste.makeVisible(this.liste.getItemCount() - 1);
      this.liste.setBackground(Color.red);
    }
  }
  
  public void send()
  {
    try
    {
      this.client.sendToServer(this.messageField.getText());
      
      this.messageField.setText("");
    }
    catch (Exception ex)
    {
      this.liste.add(ex.toString());
      this.liste.makeVisible(this.liste.getItemCount() - 1);
      this.liste.setBackground(Color.yellow);
    }
  }
  
  public void quit()
  {
    System.exit(0);
  }
  
  public static void main(String[] arg)
  {
    ClientFrame localClientFrame;
    if (arg.length == 0) {
      localClientFrame = new ClientFrame("localhost", 5555);
    }
    if (arg.length == 1) {
      localClientFrame = new ClientFrame("localhost", Integer.parseInt(arg[0]));
    }
    if (arg.length == 2) {
      localClientFrame = new ClientFrame(arg[0], Integer.parseInt(arg[1]));
    }
  }
}
