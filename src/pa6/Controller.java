package pa6;

import java.io.Serializable;
/**
 * It is the class that compose ui and processing class together.
 * @author Jidapar Jettananurak
 *
 */
public class Controller implements Serializable {
	
	private Player player1;
	private Player player2;
	private MenuUI menuUI;
	private int state;
	private UI ui;
	private Board board ;
	private Board checkBoard;
	private boolean endGame = false;
	private boolean checkPlace = true;
	
	/**
	 * Initial menu and boards.
	 */
	public Controller(){
		menuUI = new MenuUI(this);
		board = new Board();
		checkBoard = new Board();
	}
	
	/**
	 * Check have player complete.
	 * @return true if have two player, otherwise false
	 */
	public boolean isFull(){
		return player1 != null && player2 != null;
	}	
	
	/**
	 * Open the ui of board game.
	 */
	public void getStart(){
		state = 1;
		setScore();
		ui = new UI(this);
		getUI().setOpen(true);
		ui.run();
	}
	
	/**
	 * Get the board ui.
	 * @return ui of board
	 */
	public UI getUI(){
		return ui;
	}
	
	/**
	 * Get the login page.
	 * @return menuUI
	 */
	public MenuUI getMenuUI(){
		return menuUI;
	}
	
	/**
	 * Set the score of players.
	 */
	public void setScore(){
		player1.setScore(board.countScore(player1.getId()));
		player2.setScore(board.countScore(player2.getId()));
	}
	
	/**
	 * Check the game is end. If player get zero point or the board is full.
	 */
	public void checkEndGame(){
		if(player1.getScore() == 0 || player2.getScore() == 0) endGame = true;
		else if(isFullBoard())endGame = true;
	}
	
	/**
	 * Get the status of game.
	 * @return true if it ends, otherwise false
	 */
	public boolean isEndGame(){
		return endGame;
	}
	
	/**
	 * Check the board is full.
	 * @return true if it full, otherwise false
	 */
	public boolean isFullBoard(){
		for(int i = 0 ; i < board.getTable().length ; i++){
			for(int j = 0 ; j < board.getTable()[0].length ; j++ ){
				if(board.getTable()[i][j] == 0) return false;
			}
		}
		return true;
	}
	
	/**
	 * Check the player has element that can place. If it can't place ,it will be pass to next player.
	 * @param player is id of player
	 * @return true if player can place, otherwise false.
	 */
	public boolean checkAllPosition(int player ){
		checkBoard.setTable(board.getTable());
		for(int i = 0 ; i < checkBoard.getTable().length ; i++){
			for(int j = 0 ; j < checkBoard.getTable()[0].length ; j++){
				if(checkBoard.canPlace(player, i, j)){
					return true;
				}
			}
		}
		
		if(player == 1) setState(2);
		else setState(1);
		
		return false;
		
	}

	/**
	 * Create black player.
	 * @param name of player
	 */
	public void newPlayer1(String name) {
		player1 = new Player(1 , name);
		System.out.println("Controller Create Player 1");
	}
	
	/**
	 * Create white player.
	 * @param name of player
	 */
	public void newPlayer2(String name) {
		player2 = new Player(2 , name);
		System.out.println("Controller Create Player 2");
	}

	/**
	 * Get black player.
	 * @return player1
	 */
	public Player getPlayer1(){
		return player1;
	}

	/**
	 * Get white player.
	 * @return player2
	 */
	public Player getPlayer2(){
		return player2;
	}

	/**
	 * Get the board game.
	 * @return board
	 */
	public Board getBoard(){
		return board;
	}
	
	/**
	 * Get recent turn.
	 * @return id of player
	 */
	public int getState(){
		return state;
	}
	
	/**
	 * Set next turn.
	 * @param id of player
	 */
	public void setState(int id){
		state = id;
	}
	
	/**
	 * Update the images of board ui.
	 */
	public void setBoard(){
		getUI().setImage(board.getTable());
	}
	
	/**
	 * Update from the selection of player.
	 * @param player is id of player that is selected
	 * @param row is index of row
	 * @param column is index of column
	 */
	public void updateBoard(int player , int row , int column){

		if(board.canPlace(player, row, column)){
			System.out.println(getState());
			if(player == 1)	setState(2);
			else setState(1);
			board.update(player, row, column);
			if(!checkAllPosition(getState() )){
				print(getBoard().getTable());
			}
			getUI().setTurn();
			
		}
	}
	
	public void print(int[][]table){
		for(int i = 0 ; i < table.length ; i++){
			for(int j = 0 ; j < table[i].length ; j++){
				System.out.print(table[i][j]);
			}
			System.out.println();
		}
	}
	
	/**
	 * Call Login scene.
	 */
	public void run(){
		menuUI.run();
	}
	
	/**
	 * Call server scene.
	 */
	public void runServer(){
		ServerUI serverui = new ServerUI();
		serverui.run();
	}
}
