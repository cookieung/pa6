package pa6;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * It's a UI of othello board that player can play together. 
 * @author Salilthip Phuklang
 *
 */
public class UI extends JFrame implements Observer{
	
	private Controller controller;
	private Container contents;
	private JPanel panel1,scorePY1,scorePY2,status,panelGame;
	private BorderLayout board;
	private GridLayout layout;
	private ImageIcon pic,black,white,scoreWh,scoreBl,wturn,bturn;
	private Class classes;
	private JButton[][] click = new JButton[8][8];
	private JLabel title;
	private JLabel scoreBlack, b1, b2;
	private JLabel scoreWhite, w1 ,w2;
	private JLabel turn;
	private JPanel scoreB2,scoreW2;
	private int row , column;
	private boolean isOpen = false;
	private boolean selecButton = false;

	private SelectButton se;
	
	/**
	 * Constructor of this page.
	 * @param controller
	 */
	public UI(Controller controller){
		this.controller = controller;
		this.setTitle("Othello Board");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		init();
		this.pack();
	}

	/**
	 * Initial all items.
	 */
	public void init(){
		
		//Initial
		contents = new Container();
		board = new BorderLayout();
		layout = new GridLayout(8,8);
		panel1 = new JPanel();
		panel1.setBackground(Color.darkGray);
		scorePY1 = new JPanel();
		scorePY1.setBackground(Color.BLACK);
		scorePY2 = new JPanel();
		scorePY2.setBackground(Color.white);
		scorePY2.setForeground(Color.BLACK);
		status = new JPanel();
		status.setBackground(Color.darkGray);
		panelGame = new JPanel();
		classes = this.getClass();
		
		//Set Layout
		contents.setLayout(board);
		panelGame.setLayout(layout);
		
		//Add panel to container.
		contents.add(panel1,BorderLayout.PAGE_START);
		contents.add(scorePY1,BorderLayout.LINE_START);
		contents.add(panelGame,BorderLayout.CENTER);
		contents.add(scorePY2,BorderLayout.LINE_END);
		contents.add(status,BorderLayout.PAGE_END);

		URL sw = classes.getResource("/res/scoreWhite.png");
		scoreWh = new ImageIcon(sw);
		
		URL sb = classes.getResource("/res/scoreWhite.png");
		scoreBl = new ImageIcon(sb);
		

		
		//Show TEXT
		title = new JLabel("Name VS Name");
		title.setForeground(Color.WHITE);
		scoreBlack = new JLabel(scoreBl);
		scoreB2 = new JPanel(new GridLayout(1, 2));
		scoreW2 = new JPanel(new GridLayout(1,2));
		scoreBlack.setForeground(Color.WHITE);
		scoreWhite = new JLabel(scoreWh);
		scoreWhite.setForeground(Color.BLACK);
		turn = new JLabel();
		setTurn();
		turn.setForeground(Color.GREEN);
		b1= new JLabel(getImageScore(0));
		b2= new JLabel(getImageScore(2));
		scoreB2.add(b1);
		scoreB2.add(b2);
		scoreB2.setBackground(Color.black);
		w1= new JLabel(getImageScore(0));
		w2= new JLabel(getImageScore(2));
		scoreW2.add(w1);
		scoreW2.add(w2);
		scoreW2.setBackground(Color.white);

		//Add the TEXT
		panel1.add(title);
		scorePY1.add(scoreBlack);
		scorePY1.add(scoreB2);
		scorePY2.add(scoreWhite);
		scorePY2.add(scoreW2);
		status.add(turn);

		for( int i = 0 ; i < click.length ; i++ ){
			for(int j = 0 ; j < click[0].length ; j++){
				click[i][j] = new JButton();
				click[i][j].setPreferredSize(new Dimension(50, 50));
				panelGame.add( click[i][j] );
				se = new SelectButton(i,j);
				click[i][j].addActionListener(se);
				click[i][j].setOpaque(false);
			}
		}
		
		
		//Add the initial board
		this.setImage(controller.getBoard().getTable());
		this.add(contents);
		
	}
	
	/**
	 * Update the table for play.
	 * @param table is new table
	 */
	public void setImage(int [][]table){
		for( int i = 0 ; i < table.length ; i++ ){
			for(int j = 0 ; j < table[0].length ; j++){
				if( table[i][j] == 2 )
				{
					click[i][j].setIcon(this.getImageWhite());

				}
				else if( table[i][j] == 1 )
				{
					click[i][j].setIcon(this.getImageBlack());
				}
				else{
					click[i][j].setIcon(this.getImageBlock());	
				}
				
			}
		}
	}
	
	/**
	 * Get the selected row.
	 * @return the selected row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Update the row.
	 * @param the new row
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Get the selected column.
	 * @return the selected column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Update the column.
	 * @param column is new column
	 */
	public void setColumn(int column) {
		this.column = column;
	}
	
	/**
	 * Check this page is running.
	 * @return true if it is running, otherwise false
	 */
	public boolean isOpen() {
		return isOpen;
	}

	/**
	 *Update status of this page.
	 * @param isOpen is new value
	 */
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
	/**
	 * Update the status of select button.
	 * @param selectButton is required status
	 */
	public void setSelecButton(boolean selectButton){
		this.selecButton = selectButton;
	}
	
	/**
	 * Get the status of select button.
	 * @return true if it is selected, otherwise false
	 */
	public boolean isSelecButton(){
		return selecButton;
	}

	/**
	 * The class of JButton that have index.
	 * @author Salilthip Phuklang
	 *
	 */
	class CustomButton extends JButton{
		public int i,j;
		public CustomButton(int i, int j){
			this.i = i;
			this.j = j;
		}
		
		public int geti(){
			return i;
		}
		public int getj(){
			return j;
		}
	}
	
	/**
	 * The action that will do when the select button is pressed.
	 * @author Salilthip Phuklang
	 *
	 */
	public class SelectButton implements ActionListener {

		int row , column;
		/**
		 * Initial this action.
		 * @param row is pressed.
		 * @param column is pressed.
		 */
		public SelectButton(int row,int column){

			this.row = row;
			this.column = column;

		}
		
		/**
		 *  method to perform action when the button is pressed 
		 */
		public void actionPerformed(ActionEvent evt) {
			setSelecButton(true);
			controller.getUI().setRow(row);
			controller.getUI().setColumn(column);

		}
	}

	/**
	 * Get the picture of blank block.
	 * @return picture of blank block
	 */
	public ImageIcon getImageBlock(){
		URL url = classes.getResource("/res/block.png");
		pic = new ImageIcon(url);
		return pic;
	}
	
	/**
	 * Get the picture of white piece.
	 * @return picture of white piece
	 */
	public ImageIcon getImageWhite(){
		URL _white = classes.getResource("/res/white.png");
		white = new ImageIcon(_white);
		return white;
	}
	
	/**
	 * Get the picture of black piece.
	 * @return picture of black piece
	 */
	public ImageIcon getImageBlack(){
		URL _black = classes.getResource("/res/black.png");
		black = new ImageIcon(_black);
		return black;
	}
	
	/**
	 * Get this page.
	 * @return this class
	 */
	public UI getUI(){
		return this;
	}
	
	/**
	 * Change the picture of turn.
	 */
	public void setTurn(){
		URL rn = classes.getResource("/res/setWHITEturn.png");
		wturn = new ImageIcon(rn);
		URL bt = classes.getResource("/res/setBlackTurn.png");
		bturn = new ImageIcon(bt);
		if(controller.getState() == 1){
			turn.setIcon(bturn);

		}
		else{
			turn.setIcon(wturn);

		}
	}
	
	/**
	 * Show this page.
	 */
	public void run(){
		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("");
	}
	
	/**
	 * Change the score number.
	 * @param i is the number that want.
	 * @return the picture of score number that is required
	 */
	public ImageIcon getImageScore(int i){
		String s = "/res/"+i+".png/";
		URL url = classes.getResource(s);
		return new ImageIcon(url);
	}
	
	/**
	 * Update the Score label
	 * @param py1 is score of black player
	 * @param py2 is score of white player
	 */
	public void setScoreLabel(int py1,int py2){

		b1.setIcon(getImageScore(py1/10));
		b2.setIcon(getImageScore(py1%10));
		w1.setIcon(getImageScore(py2/10));
		w2.setIcon(getImageScore(py2%10));
	}
	
}
