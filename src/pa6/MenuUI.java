package pa6;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * It's the UI of login page for player.
 * @author Salilthip Phuklang
 *
 */
public class MenuUI extends JFrame implements Observer{

	private Controller controller;
	private String name;
	private boolean pressLogin = false;
	private boolean pressStart = false;
	private URL urll;
	private ImageIcon i;
	
	private JPanel contents;
	private JLabel login,start,head,bg;
	private JTextField player;
	private BoxLayout layout;
	private Class c;
	private ImageIcon imgLogin,imgStart,img;
	
	/**
	 * Initial this page by controller class.
	 * @param controller is an operation class
	 */
	public MenuUI(Controller controller){
		this.controller = controller;
		this.setTitle("Menu Login");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		init();

	}

	/**
	 * Initial the user interface of application.
	 */
	public void init(){
	
		contents = new JPanel();
		setBounds(-5, -10, 800, 600);
		contents.setLayout(null);
		c = this.getClass();
		
		contents.setBackground(Color.BLUE);
		setContentPane(contents);
		contents.setLayout(layout);

		urll = c.getResource("/res/newBackground.png");
		i = new ImageIcon(urll);
		
		
		player = new JTextField(8);
		player.setBounds(134, 447, 153, 43);
		contents.add(player);
		player.setColumns(10);
		
		URL url = c.getResource("/res/head.png");
		img = new ImageIcon(url);
		
		URL url1 = c.getResource("/res/loginButton.png");
		imgLogin = new ImageIcon(url1);
		URL url2 = c.getResource("/res/startButton.png");
		imgStart = new ImageIcon(url2);
		
		head = new JLabel(i);
		login = new JLabel(imgLogin);
		start = new JLabel(imgStart);

		
		LoginPress loginPress = new LoginPress();
		login.addMouseListener(loginPress);
		login.setBounds(328, 434, 162, 71);
		contents.add(login);
		
		StartPress startPress = new StartPress();
		start.setBounds(563, 434, 153, 71);
		start.addMouseListener(startPress);
		contents.add(start);
		
		bg = new JLabel(i);
		bg.setBounds(-5, -10, 800, 600);
		contents.add(bg);
		
	}
	
	/**
	 * Check if the login button is pressed.
	 * @return true if it is pressed, otherwise false
	 */
	public boolean isPressLogin() {
		return pressLogin;
	}

	/**
	 * Change the state of login pressing.
	 * @param pressLogin is new state.
	 */
	public void setPressLogin(boolean pressLogin) {
		this.pressLogin = pressLogin;
	}

	/**
	* Check if the start button is pressed.
	 * @return true if it is pressed, otherwise false
	 */
	public boolean isPressStart() {
		return pressStart;
	}

	/**
	  * Change the state of start pressing.
	 * @param pressStart is new state.
	 */
	public void setPressStart(boolean pressStart) {
		this.pressStart = pressStart;
	}

	/**
	 * Setting action when the mouse click on login button. 
	 * @author Salilthip Phuklang
	 *
	 */
	public class LoginPress implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
		
		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {
			
			setPressLogin(true);
			name = player.getText();

			
		}

		
	}
	
	/**
	 * Get the name from JtextField.
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Setting action when the mouse click on start button. 
	 * @author Salilthip Phuklang
	 *
	 */
	public class StartPress implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
		
		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {
			setPressStart(true);
			
		}

	}
	
	/**
	 * Get this page.
	 * @return this frame
	 */
	public JFrame getMenu(){
		return this;
	}
	
	/**
	 * Show this page.
	 */
	public void run(){
		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
	
}
