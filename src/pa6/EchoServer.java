package pa6;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Server that operate the system of game.
 * @author Jidapar Jettananurak
 *
 */
public class EchoServer extends AbstractServer{

	private static final int PORT = 5555;
	private Controller controller;
	
	/**
	 * Initial server by port number.
	 * @param port
	 */
	public EchoServer(int port) {
		super(port);
		
		controller = new Controller();
		controller.runServer();
	}

	/**
	 * Receive message from connected clients.
	 */
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {

		String m = (String)msg;

		if(controller.getPlayer1() == null){
			controller.newPlayer1(m);
			System.out.println("Server create : player1");
			super.sendToAllClients(m);
			sendToClient(client , "PLAYER1");
		}else if(controller.getPlayer2() == null){
			controller.newPlayer2(m);
			System.out.println("Server create : player2");
			super.sendToAllClients(m);
			sendToClient(client , "PLAYER2");
		}else if( m.equalsIgnoreCase("play") ){
			super.sendToAllClients("START");
		}else if( m.equalsIgnoreCase("end") ){
			super.sendToAllClients("END");
		}
		else super.sendToAllClients(m);
		
	}
	
	/**
	 * Send message one-one to client.
	 * @param client is terminal client
	 * @param str is required object 
	 */
	public void sendToClient(ConnectionToClient client , Object str){
		try{
			client.sendToClient(str);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		 EchoServer server = new EchoServer(PORT);
		 try {
			 server.listen();
			 System.out.printf("Listening on port %d\n",PORT);
			 
		 } catch (IOException e) {
			 System.out.println("Couldn't start server:");
			 System.out.println(e);
			 }
		}

}
