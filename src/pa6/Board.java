package pa6;

import java.io.Serializable;
import java.util.Scanner;

/**
 * It is othello board class.
 * @author Jidapar Jettananurak
 *
 */
public class Board implements Serializable {
	
	private int table[][];
	private boolean r;
	private boolean l;
	private boolean u;
	private boolean d;
	private boolean ur;
	private boolean dr;
	private boolean ul;
	private boolean dl;
	
	/**
	 * Initial the board.
	 */
	public Board(){
		setNewBoard();
	}
	
	/**
	 * Get the array of the board.
	 * @return array of recent board
	 */
	public int[][] getTable() {
		return table;
	}

	/**
	 * Set board with new table.
	 * @param table is required
	 */
	public void setTable(int[][] table) {
		this.table = table;
	}
	
	/**
	 * Set the table to original board.
	 */
	public void setNewBoard(){
		table = new int[][] { 
				{0, 0, 0, 0, 0, 0, 0, 0 } ,
				{0, 0, 0, 0, 0, 0, 0, 0 } ,
				{0, 0, 0, 0, 0, 0, 0, 0 } ,
				{0, 0, 0, 1, 2, 0, 0, 0 } ,
				{0, 0, 0, 2, 1, 0, 0, 0 } ,
				{0, 0, 0, 0, 0, 0, 0, 0 } ,
				{0, 0, 0, 0, 0, 0, 0, 0 } ,
				{0, 0, 0, 0, 0, 0, 0, 0 } };
	}

	/**
	 * Update the new board that player choose position.
	 * @param player is id of player.
	 * @param row is index of row board that it is pressed, it start with 0 to 7.
	 * @param column is index of column board that it is pressed, it start with 0 to 7.
	 */
	public void update(int player , int row , int column){
		int newTable[][] = table;
		int checkRow = row;
		int checkColumn = column;
		
		if( canPlace(player, row, column) ){
			newTable[row][column] = player;
			
			if( dl ){
				System.out.println(dl + " bug2");
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow+1 ; i < newTable.length ; i++){
					checkColumn--;
					if(checkColumn > -1){
						if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
						else newTable[i][checkColumn] = player;
					}
				}
				dl = false;
			}
			if( r ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkColumn+1 ; i < newTable[0].length ; i++){
					if( newTable[checkRow][i] == 0 || newTable[checkRow][i] == player ) break;
					else newTable[checkRow][i] = player;
				}
				r = false;
			}
			if( l ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkColumn-1 ; i > -1 ; i--){
					if( newTable[checkRow][i] == 0 || newTable[checkRow][i] == player ) break;
					else newTable[checkRow][i] = player;
				}
				l = false;
			}
			if( d ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow+1 ; i < newTable.length ; i++){
					if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
					else newTable[i][checkColumn] = player;
				}
				d = false;
			}
			if( u ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow-1 ; i > -1 ; i--){
					if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
					else newTable[i][checkColumn] = player;
				}
				u = false;
			}
			if( ur ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow-1 ; i > -1 ; i--){
					checkColumn++;
					if(checkColumn < newTable[0].length){
						if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
						else newTable[i][checkColumn] = player;
					}
				}
				ur = false;
			}
			if( dr ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow+1 ; i < newTable.length ; i++){
					checkColumn++;
					if(checkColumn < newTable[0].length){
						if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
						else newTable[i][checkColumn] = player;
					}
				}
				dr = false;
			}
			if( ul ){
				checkRow = row;
				checkColumn = column;
				for(int i = checkRow-1 ; i > -1 ; i--){
					checkColumn--;
					if(checkColumn > -1){
						if( newTable[i][checkColumn] == 0 || newTable[i][checkColumn] == player ) break;
						else newTable[i][checkColumn] = player;
					}
				}
				ul = false;
			}
			this.table = newTable;
		}
	}
	
	/**
	 * Check the element that player select.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 * @return true if element can be placed, otherwise is false
	 */
	public boolean canPlace(int player , int row , int column){
		return table[row][column] == 0  && canEat(player, row, column);
	}
	
	/**
	 * Check element that player can strike other player.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 * @return false if there isn't any element that can be strike, otherwise true
	 */
	public boolean canEat(int player , int row , int column){
		
		r = false;
		l = false;
		u = false;
		d = false;
		ur = false;
		ul = false;
		dr = false;
		dl = false;
		
		rightBound(player , row , column);
		leftBound(player, row, column);
		downBound(player, row, column);
		upBound(player, row, column);
		upRight(player, row, column);
		downRight(player, row, column);
		upLeft(player, row, column);
		downLeft(player, row, column);
		
		return r || l || d || u || ur || dr || ul || dl;
	}
	
	/**
	 * Check the east line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void rightBound(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkColumn+1 ; i < table[0].length ; i++){
			if( table[checkRow][i] == player ){
				me++;
				break;
			}else if( table[checkRow][i] == 0 ){
				break;
			}else check++;
		}
		
		if(me == 1 && check > 0) r = true;
	}
	
	/**
	 * Check the west line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void leftBound(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkColumn-1 ; i > -1 ; i--){
			if( table[checkRow][i] == player ){
				me++;
				break;
			}else if( table[checkRow][i] == 0 ){
				break;
			}else check++;
		}
		
		if(me == 1 && check > 0) l =  true;
	}
	
	/**
	 * Check the south line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void downBound(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow+1 ; i < table.length ; i++){
			if( table[i][checkColumn] == player ){
				me++;
				break;
			}else if( table[i][checkColumn] == 0 ){
				break;
			}else check++;
		}
		
		if(me == 1 && check > 0) d = true;
	}
	
	/**
	 * Check the north line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void upBound(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow-1 ; i > -1 ; i--){
			if( table[i][checkColumn] == player ){
				me++;
				break;
			}else if( table[i][checkColumn] == 0 ){
				break;
			}else check++;
		}
		
		if(me == 1 && check > 0) u = true;
	}
	
	/**
	 * Check the north of east line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void upRight(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow-1 ; i > -1 ; i--){
			checkColumn++;
			if(checkColumn < table[0].length){
				if( table[i][checkColumn] == player ){
					me++;
					break;
				}else if( table[i][checkColumn] == 0 ){
					break;
				}else check++;
			}
		}
		
		if(me == 1 && check > 0) ur =  true;
	}
	
	/**
	 * Check the south of east line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void downRight(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow+1 ; i < table.length ; i++){
			checkColumn++;
			if(checkColumn < table[0].length){
				if( table[i][checkColumn] == player ){
					me++;
					break;
				}else if( table[i][checkColumn] == 0 ){
					break;
				}else check++;
			}
		}
		
		if(me == 1 && check > 0) dr = true;
	}
	
	/**
	 * Check the north of west line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void upLeft(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow-1 ; i > -1 ; i--){
			checkColumn--;
			if(checkColumn > -1){
				if( table[i][checkColumn] == player ){
					me++;
					break;
				}else if( table[i][checkColumn] == 0 ){
					break;
				}else check++;
			}
		}
		
		if(me == 1 && check > 0) ul = true;
	}
	
	/**
	 * Check the south of west line of selected point.
	 * @param player is id of player
	 * @param row is index of row 
	 * @param column is index of column
	 */
	public void downLeft(int player , int row , int column){
		int check = 0;
		int me = 0;
		int checkRow = row;
		int checkColumn = column;
		for(int i = checkRow+1 ; i < table.length ; i++){
			checkColumn--;
			if(checkColumn > -1){
				if( table[i][checkColumn] == player ){
					me++;
					break;
				}else if( table[i][checkColumn] == 0 ){
					break;
				}else check++;
			}
		}
		
		if(me == 1 && check > 0) dl = true;
	}
	
	/**
	 * Count the score of the requirement.
	 * @param player is id of player
	 * @return the require score
	 */
	public int countScore(int player){
		int score = 0;
		for(int i = 0 ; i < table.length ; i++){
			for(int j = 0 ; j < table[0].length ; j++){
				if(table[i][j] == player) score++;
			}
		}
		return score;
	}
	
}
