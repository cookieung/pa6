package pa6;

import java.io.Serializable;

/**
 * Class of one player.
 * @author Jidapar Jettananurak
 *
 */
public class Player implements Serializable{
	
	private int id;
	private String name;
	private int score;
	
	/**
	 * Initial id and name of player.
	 * @param id of player
	 * @param name of player
	 */
	public Player(int id , String name){
		this.id = id;
		this.name = name;
		score = 0;
	}
	
	/**
	 * Get id of player.
	 * @return id of player
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Get name of player.
	 * @return name of player
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Get score of player.
	 * @return score of player
	 */
	public int getScore(){
		return score;
	}
	
	/**
	 * Set new score of player.
	 * @param score is the new score 
	 */
	public void setScore(int score){
		this.score = score;
	}

}
