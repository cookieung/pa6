package pa6;

import java.awt.Container;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Only UI that show that server is running.
 * @author Salilthip Phuklang
 *
 */
public class ServerUI extends JFrame{

	private Container contents;
	private JPanel panel;
	private JLabel label;
	private JTextArea text;
	private BoxLayout layout;
	
	/**
	 * Initial Server page
	 */
	public ServerUI(){
		this.setTitle("Server");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		init();
	}
	
	/**
	 * Create all items.
	 */
	public void init(){
		contents = new Container();
		label = new JLabel("Server");
		text = new JTextArea();
		text.setEditable(false);
		layout = new BoxLayout(contents,BoxLayout.Y_AXIS);
		contents.setLayout(layout);
		contents.add(label);
		contents.add(text);
		this.add(contents);
		this.setSize(500, 400);
	}
	
	/**
	 * Show this page.
	 */
	public void run(){
		this.setVisible(true);
	}
	
}
